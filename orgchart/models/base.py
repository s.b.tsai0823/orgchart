from orgchart.core.exceptions import FieldDoesNotExist
from orgchart.models.fields import Field, RelatedField
from orgchart.manager import JsonFileManager


class MetaModel(type):
    manager_class = JsonFileManager

    def _get_manager(cls):
        return cls.manager_class(model_class=cls)

    @property
    def objects(cls):
        #print('MetaModel.objects')
        return cls._get_manager()


class ModelBase(metaclass=MetaModel):
    """ Base class for all models """
    __meta = {}
    
    def __new__(cls,data_dict):
        #print('__new__ called')
        inst = super(ModelBase, cls).__new__(cls)
        for attr_str in dir(inst):
            if attr_str.startswith('__'):
                continue
            attr_value = getattr(inst, attr_str)
            field_value = None
            if isinstance(attr_value, Field):
                if attr_value.primary:
                    inst.__meta['primary_key'] = attr_str

                db_column = attr_str
                #print(attr_str )
                if attr_value.db_column:
                    db_column = attr_value.db_column

                if db_column in data_dict:
                    field_value = data_dict[db_column]
                    setattr(inst, attr_str, field_value)
                else:
                    raise FieldDoesNotExist(
                        f"{attr_str} does not exist in source data.")

            if isinstance(attr_value, RelatedField):
                #print('related model', attr_value.related_model)
                
                query_dict = {}
                query_dict[attr_value.lookup_column] = field_value
                #print('query_dict',query_dict)
                relmod = attr_value.related_model
                if isinstance(relmod, str) and relmod == "self":
                    relmod = cls
                
                #print(relmod)
                field_value = relmod.objects.get(**query_dict)
                
                setattr(inst, attr_str, field_value)
                #print(field_value)
        return inst

    # def __init__(self, manager_instance, data_dict):
    #     self.objects = manager_instance
    #     for attr_str in dir(self):
    #         if attr_str.startswith('__'):
    #             continue
    #         attr_value = getattr(self, attr_str)
    #         field_value = None
    #         if isinstance(attr_value, Field):
    #             if attr_value.primary:
    #                 self.__meta['primary_key'] = attr_str
                
    #             db_column = attr_str
    #             #print(attr_str )
    #             if attr_value.db_column:
    #                 db_column = attr_value.db_column

    #             if db_column in data_dict:
    #                 field_value = data_dict[db_column]
    #                 setattr(self, attr_str, field_value)
    #             else:
    #                 raise FieldDoesNotExist(
    #                     f"{attr_str} does not exist in source data.")

    #         if isinstance(attr_value, RelatedField):
    #             print('related model', attr_value.related_model)
    #             query_dict = {}
    #             query_dict[attr_value.lookup_column] = field_value
    #             print(query_dict)
    #             relmod = attr_value.related_model()
    #             field_value = relmod.objects.get(query_dict)
                #field_value = 

    def __repr__(self):
        return self.__str__()
