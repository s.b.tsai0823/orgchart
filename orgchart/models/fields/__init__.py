class Field():
    """Base class for all field types"""
    primary = False
    db_column = None
    
    def __init__(self,primary=False,db_column=None):
        self.primary = primary
        if db_column:
            self.db_column = db_column


class RelatedField(Field):
    """relational fields"""
    related_model = None
    lookup_column = None

    def __init__(self, related_model, lookup_column=None, **kwargs):
        self.related_model = related_model
        self.lookup_column = lookup_column
        super().__init__(**kwargs)
