
class FieldDoesNotExist(Exception):
    """model field does not exist in source data"""
    pass
