from orgchart.models.base import ModelBase
from orgchart.models import fields

class Employees(ModelBase):
    id = fields.Field(primary=True)
    employee_no = fields.Field()
    employee_name = fields.Field()
    email = fields.Field()
    onboarding_date = fields.Field()
    organization_id = fields.Field()
    job_title = fields.Field()
    gender = fields.Field()

    def __str__(self):
        return f"<{self.__class__.__name__}(id={self.id}): {self.email}>"

    class Meta:
        db_table = "employees.json"


class Organization(ModelBase):
    id = fields.Field(primary=True)
    organization_name = fields.Field()
    parent = fields.RelatedField(
        "self",
        lookup_column='id',
        db_column='parent_id')
    organization_code = fields.Field()
    organization_name = fields.Field()
    supervisor = fields.RelatedField(
        Employees, 
        lookup_column='employee_no', 
        db_column='supervisor_id')
    is_active = fields.Field()

    def __str__(self):
        return f"<{self.__class__.__name__}(id={self.id}): {self.organization_name}>"
    
    class Meta:
        db_table = "organizations.json"



