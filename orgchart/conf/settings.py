from pathlib import Path


BASE_DIR = Path(__file__).resolve().parent.parent

SOURCE_FILE_DIR = BASE_DIR / 'source'
