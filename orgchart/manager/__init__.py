import json
import abc
from pathlib import Path
from orgchart.conf import settings as default_settings
#from orgchart.models.manager.json import JsonFileManager

class ManagerBase(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def list(self):
        return NotImplemented

    @abc.abstractmethod
    def get(self, pk):
        return NotImplemented


class JsonFileManager(ManagerBase):
    model = None
    source_file_path = None
    source_data = []

    def __init__(self,model_class=None):
        self.model = model_class
        file_name = self.model.Meta.db_table
        self.source_file_path = default_settings.SOURCE_FILE_DIR / file_name
        with open(self.source_file_path) as f:
            self.source_data = json.load(f)
        

    def list(self):
        ret = []
        for data_dict in self.source_data:
            data_instance = self.model(data_dict)
            ret.append(data_instance)
        return ret

    def get(self, **kwargs):
        data_instance = None
        for data_dict in self.source_data:
            match = True
            for k, v in kwargs.items():
                if data_dict[k] != v:
                    match = False
                    break

            if match:
                data_instance = self.model(data_dict)
                break

        return data_instance
